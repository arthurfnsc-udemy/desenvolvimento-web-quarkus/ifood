# iFood

Projeto do curso Desenvolvimento
[Web com Quarkus](https://www.udemy.com/course/des-web-quarkus/) ministrado
por [Vinicius Ferraz Campos Florentino](https://www.linkedin.com/in/viniciusfcf/)

## Ementa Vinícius Ferraz Campos Florentino

Neste curso irei abordar o Quarkus: framework que irá revolucionar o mundo Java
e de microsserviços que rodam em nuvem. Quarkus é um framework Open Source
mantido pela Red Hat que torna o desenvolvimento em Java divertido, rápido,
com utilização de memória incrivelmente baixo e tempo de boot incrivelmente
rápido.

É o framework ideal para desenvolvimento de aplicações que rodarão em nuvem:
Azure, AWS e Google Cloud.

Apresento exemplos concretos e aplicáveis em ambiente produtivo.

Desenvolveremos três microsserviços e conheceremos as extensões mais
importantes do Quarkus para desenvolvimento com Qualidade.

Irei apresentar as mais diversas extensões, bibliotecas e tecnologias
utilizadas atualmente.

Código fonte disponível no GitHub: [viniciusfcf/udemy-quarkus](https://github.com/viniciusfcf/udemy-quarkus)

## Considerações Arthur Magalhães Fonseca

Adaptarei algumas partes do cursossem mudar suas funcionalidades. A seguir
seguem as principais mudanças:

- Utilização de [Gradle](https://gradle.org) como gerenciador de dependências
- Utilização de [Java 17](https://adoptium.net/temurin/releases)
- Utilização de plugin [Markdown Lint](https://github.com/appmattus/markdown-lint)
- Utilização de plugin [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator)
- Utilização de
  plugin [OWASP Dependency Check](https://jeremylong.github.io/DependencyCheck/dependency-check-gradle/index.html)
- Utilização de plugin [Spotless](https://github.com/diffplug/spotless/tree/main/plugin-gradle)
  com [Prettier](https://github.com/jhipster/prettier-java)
- Utilização de plugin [Versions](https://github.com/ben-manes/gradle-versions-plugin)
- Utilização de Pré Commit Hook

### Pendências

- Implementação de validação com ConstraintValidatorContext utilizando JPA
- Aplicar mapstruct em projeto marketplace

### Execução de projeto de cadastro de restaurantes e pratos

```
foo@bar:~$ docker-compose up grafana_ifood jaeger_ifood keycloak_ifood postgres_cadastro_ifood prometheus_ifood
foo@bar:~$ ./gradlew :applications:cadastro:quarkusDev
```

### Execução de projeto de marketplace

```
foo@bar:~$ docker-compose up postgres_marketplace_ifood
foo@bar:~$ ./gradlew :applications:marketplace:quarkusDev
```