package com.gitlab.arthurfnsc.ifood.marketplace;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.util.HashMap;
import java.util.Map;
import org.testcontainers.containers.PostgreSQLContainer;

public class MarketplaceTestLifecycleManager
    implements QuarkusTestResourceLifecycleManager {
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>(
        "postgres:15.1-alpine"
    );

    @Override
    public Map<String, String> start() {
        POSTGRES.withInitScript("cadastro.sql").start();
        Map<String, String> propriedades = new HashMap<>();
        propriedades.put(
            "quarkus.datasource.reactive.url",
            String.format(
                "postgresql://%s:%d/%s",
                POSTGRES.getHost(),
                POSTGRES.getFirstMappedPort(),
                POSTGRES.getDatabaseName()
            )
        );
        propriedades.put("quarkus.datasource.username", POSTGRES.getUsername());
        propriedades.put("quarkus.datasource.password", POSTGRES.getPassword());
        return propriedades;
    }

    @Override
    public void stop() {
        if (POSTGRES.isRunning()) {
            POSTGRES.stop();
        }
    }
}
