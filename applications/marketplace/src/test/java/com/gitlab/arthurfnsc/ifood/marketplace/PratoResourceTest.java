package com.gitlab.arthurfnsc.ifood.marketplace;

import static io.restassured.RestAssured.given;
import static org.approvaltests.JsonApprovals.verifyJson;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import javax.ws.rs.core.Response.Status;
import org.junit.jupiter.api.Test;

@QuarkusTestResource(MarketplaceTestLifecycleManager.class)
@QuarkusTest
class PratoResourceTest {

    @Test
    public void deveBuscarPratos() {
        String resultado = given()
            .log()
            .all()
            .when()
            .get("/pratos")
            .then()
            .statusCode(Status.OK.getStatusCode())
            .extract()
            .asString();
        verifyJson(resultado);
    }
}
