insert
    into
        localizacao(
            id,
            latitude,
            longitude
        )
    values(
        1000,
        - 15.817759,
        - 47.836959
    );

insert
    into
        restaurante(
            id,
            localizacao_id,
            nome
        )
    values(
        999,
        1000,
        'Manguai'
    );

insert
    into
        prato(
            id,
            nome,
            descricao,
            restaurante_id,
            preco
        )
    values(
        9998,
        'Cuscuz com Ovo',
        'Bom demais no café da manhã',
        999,
        3.99
    );

insert
    into
        prato(
            id,
            nome,
            descricao,
            restaurante_id,
            preco
        )
    values(
        9997,
        'Peixe frito',
        'Agulhinha frita, excelente com Cerveja',
        999,
        99.99
    );