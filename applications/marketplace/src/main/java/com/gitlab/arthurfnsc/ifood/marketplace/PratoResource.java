package com.gitlab.arthurfnsc.ifood.marketplace;

import io.smallrye.mutiny.Multi;
import java.util.List;
import java.util.concurrent.CompletionStage;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.openapi.server.v1.pratos.api.PratosApi;
import org.openapi.v1.pratos.model.Prato;

@Path("/pratos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PratoResource implements PratosApi {

    @Override
    public CompletionStage<List<Prato>> recuperaPratosRestaurante() {
        Multi<Prato> pratos = com.gitlab.arthurfnsc.ifood.marketplace.Prato.recuperaPratosApi();
        return pratos.collect().asList().subscribeAsCompletionStage();
    }
}
