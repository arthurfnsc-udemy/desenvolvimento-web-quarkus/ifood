package com.gitlab.arthurfnsc.ifood.marketplace;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.smallrye.mutiny.Multi;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prato")
public class Prato extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    public String nome;

    public String descricao;

    @ManyToOne
    public Restaurante restaurante;

    public BigDecimal preco;

    public static Multi<org.openapi.v1.pratos.model.Prato> recuperaPratosApi() {
        return findAll()
            .stream()
            .map(p -> (Prato) p)
            .onItem()
            .transform(PratoMapper::paraPratoApi);
    }

    public static Multi<org.openapi.v1.restaurantes.model.Prato> recuperaPratosApi(
        Long idRestaurante
    ) {
        return find("restaurante.id", idRestaurante)
            .stream()
            .map(p -> (Prato) p)
            .onItem()
            .transform(PratoMapper::paraPratoRestauranteApi);
    }
}
