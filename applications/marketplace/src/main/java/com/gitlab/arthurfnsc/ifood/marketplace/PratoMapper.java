package com.gitlab.arthurfnsc.ifood.marketplace;

public class PratoMapper {

    public static org.openapi.v1.pratos.model.Prato paraPratoApi(Prato prato) {
        org.openapi.v1.pratos.model.Prato resultado = new org.openapi.v1.pratos.model.Prato();
        resultado.setId(prato.id);
        resultado.setDescricao(prato.descricao);
        resultado.setNome(prato.nome);
        resultado.setPreco(prato.preco.doubleValue());
        return resultado;
    }

    public static org.openapi.v1.restaurantes.model.Prato paraPratoRestauranteApi(
        Prato prato
    ) {
        org.openapi.v1.restaurantes.model.Prato resultado = new org.openapi.v1.restaurantes.model.Prato();
        resultado.setId(prato.id);
        resultado.setDescricao(prato.descricao);
        resultado.setNome(prato.nome);
        resultado.setPreco(prato.preco.doubleValue());
        return resultado;
    }
}
