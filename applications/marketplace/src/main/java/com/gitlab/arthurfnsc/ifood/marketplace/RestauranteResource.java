package com.gitlab.arthurfnsc.ifood.marketplace;

import io.smallrye.mutiny.Multi;
import java.util.List;
import java.util.concurrent.CompletionStage;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.openapi.server.v1.restaurantes.api.RestaurantesApi;

@Path("/restaurantes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestauranteResource implements RestaurantesApi {

    @Override
    public CompletionStage<List<org.openapi.v1.restaurantes.model.Prato>> recuperaPratosRestaurante(
        Long idRestaurante
    ) {
        Multi<org.openapi.v1.restaurantes.model.Prato> pratos = com.gitlab.arthurfnsc.ifood.marketplace.Prato.recuperaPratosApi(
            idRestaurante
        );
        return pratos.collect().asList().subscribeAsCompletionStage();
    }
}
