package com.gitlab.arthurfnsc.ifood.cadastro;

import static org.approvaltests.JsonApprovals.verifyJson;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;
import com.gitlab.arthurfnsc.ifood.cadastro.util.TokenUtils;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;
import javax.ws.rs.core.Response.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(CadastroTestLifecycleManager.class)
class RestauranteResourceTest {
    private String token;

    @BeforeEach
    public void gereToken() throws Exception {
        token =
            TokenUtils.generateTokenString("/JWTProprietarioClaims.json", null);
    }

    private RequestSpecification given() {
        return RestAssured
            .given()
            .contentType(ContentType.JSON)
            .header(new Header("Authorization", "Bearer " + token));
    }

    @Test
    @DataSet("restaurantes-cenario-1.yaml")
    @DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
    public void deveBuscarRestaurantes() {
        String resultado = given()
            .log()
            .all()
            .when()
            .get("/restaurantes")
            .then()
            .statusCode(Status.OK.getStatusCode())
            .extract()
            .asString();
        verifyJson(resultado);
    }

    @Test
    @DataSet("restaurantes-cenario-1.yaml")
    public void deveAlterarRestaurante() {
        Restaurante dto = new Restaurante();
        dto.nome = "novoNome";
        Long parameterValue = 123L;
        given()
            .with()
            .pathParam("id", parameterValue)
            .body(dto)
            .log()
            .all()
            .when()
            .put("/restaurantes/{id}")
            .then()
            .statusCode(Status.NO_CONTENT.getStatusCode())
            .extract()
            .asString();
        Restaurante findById = Restaurante.findById(parameterValue);
        assertEquals(dto.nome, findById.nome);
    }
}
