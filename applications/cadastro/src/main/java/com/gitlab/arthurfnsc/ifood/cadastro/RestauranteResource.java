package com.gitlab.arthurfnsc.ifood.cadastro;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.SimplyTimed;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlow;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlows;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.openapi.server.v1.restaurantes.api.RestaurantesApi;
import org.openapi.v1.restaurantes.model.AtualizacaoPrato;
import org.openapi.v1.restaurantes.model.AtualizacaoRestaurante;
import org.openapi.v1.restaurantes.model.CadastroPrato;
import org.openapi.v1.restaurantes.model.CadastroRestaurante;

@Path("/restaurantes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("proprietario")
@SecurityScheme(
    securitySchemeName = "ifood-oauth",
    type = SecuritySchemeType.OAUTH2,
    flows = @OAuthFlows(password = @OAuthFlow(tokenUrl = ""))
)
@SecurityRequirement(name = "ifood-oauth", scopes = {})
public class RestauranteResource implements RestaurantesApi {
    @Inject
    PratoMapper pratoMapper;

    @Inject
    RestauranteMapper restauranteMapper;

    @Override
    public Response atualizaPratoRestaurante(
        Long idPrato,
        Long idRestaurante,
        AtualizacaoPrato atualizacaoPrato
    ) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        if (restauranteOp.isEmpty()) {
            throw new NotFoundException("Restaurante não existe");
        }
        Optional<Prato> pratoOp = Prato.findByIdOptional(idPrato);
        if (pratoOp.isEmpty()) {
            throw new NotFoundException("Prato não existe");
        }
        Prato prato = pratoOp.get();
        prato.descricao = atualizacaoPrato.getDescricao();
        prato.nome = atualizacaoPrato.getNome();
        prato.preco = BigDecimal.valueOf(atualizacaoPrato.getPreco());
        prato.persist();
        return Response.noContent().build();
    }

    @Override
    @Transactional
    public Response atualizaRestaurante(
        Long idRestaurante,
        AtualizacaoRestaurante atualizacaoRestaurante
    ) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        if (restauranteOp.isEmpty()) {
            throw new NotFoundException();
        }
        Restaurante restaurante = restauranteOp.get();
        restaurante.nome = atualizacaoRestaurante.getNome();
        restaurante.persist();
        return Response.noContent().build();
    }

    @Override
    @Transactional
    public Response cadastraPratoRestaurante(
        Long idRestaurante,
        CadastroPrato cadastroPrato
    ) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        if (restauranteOp.isEmpty()) {
            throw new NotFoundException("Restaurante não existe");
        }
        Prato prato = pratoMapper.paraPrato(cadastroPrato);
        prato.persist();
        return Response.status(Response.Status.CREATED).build();
    }

    @Override
    @Transactional
    public Response cadastraRestaurante(
        CadastroRestaurante cadastroRestaurante
    ) {
        Restaurante restaurante = restauranteMapper.paraRestaurante(
            cadastroRestaurante
        );
        restaurante.persist();
        return Response.status(Response.Status.CREATED).build();
    }

    @Override
    @Counted(name = "Quantidade buscas restaurante")
    @SimplyTimed(name = "Tempo simples de busca")
    @Timed(name = "Tempo completo de busca")
    public Response recuperaPratosRestaurante(Long idRestaurante) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        if (restauranteOp.isEmpty()) {
            throw new NotFoundException("Restaurante não existe");
        }
        List<Prato> pratos = Prato.list("restaurante", restauranteOp.get());
        return Response.ok(pratoMapper.paraListaPratoApi(pratos)).build();
    }

    @Override
    public Response recuperaRestaurantes() {
        List<Restaurante> restaurantes = Restaurante.listAll();
        return Response
            .ok(restauranteMapper.paraListaRestauranteApi(restaurantes))
            .build();
    }

    @Override
    @Transactional
    public Response removePratoRestaurante(Long idPrato, Long idRestaurante) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        if (restauranteOp.isEmpty()) {
            throw new NotFoundException("Restaurante não existe");
        }
        Optional<Prato> pratoOp = Prato.findByIdOptional(idPrato);
        pratoOp.ifPresentOrElse(
            Prato::delete,
            () -> {
                throw new NotFoundException();
            }
        );
        return Response.noContent().build();
    }

    @Override
    @Transactional
    public Response removeRestaurante(Long idRestaurante) {
        Optional<Restaurante> restauranteOp = Restaurante.findByIdOptional(
            idRestaurante
        );
        restauranteOp.ifPresentOrElse(
            Restaurante::delete,
            () -> {
                throw new NotFoundException();
            }
        );
        return Response.noContent().build();
    }
}
