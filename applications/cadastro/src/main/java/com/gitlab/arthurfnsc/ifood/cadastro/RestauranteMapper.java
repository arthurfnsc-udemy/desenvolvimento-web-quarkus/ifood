package com.gitlab.arthurfnsc.ifood.cadastro;

import java.time.OffsetTime;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.openapi.v1.restaurantes.model.CadastroRestaurante;

@Mapper(componentModel = "cdi", imports = { OffsetTime.class })
public interface RestauranteMapper {
    @Mappings(
        value = {
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "dataAtualizacao", ignore = true),
            @Mapping(target = "dataCriacao", ignore = true),
            @Mapping(source = "latitude", target = "localizacao.latitude"),
            @Mapping(source = "longitude", target = "localizacao.longitude"),
        }
    )
    Restaurante paraRestaurante(CadastroRestaurante cadastroRestaurante);

    @Mappings(
        value = {
            @Mapping(
                target = "dataAtualizacao",
                expression = "java(restaurante.dataAtualizacao == null ? null : restaurante.dataAtualizacao.atTime(OffsetTime.now()))"
            ),
            @Mapping(
                target = "dataCriacao",
                expression = "java(restaurante.dataCriacao == null ? null : restaurante.dataCriacao.atTime(OffsetTime.now()))"
            ),
            @Mapping(source = "localizacao.latitude", target = "latitude"),
            @Mapping(source = "localizacao.longitude", target = "longitude"),
        }
    )
    org.openapi.v1.restaurantes.model.Restaurante paraRestauranteApi(
        Restaurante restaurante
    );

    List<org.openapi.v1.restaurantes.model.Restaurante> paraListaRestauranteApi(
        List<Restaurante> restaurantes
    );
}
