package com.gitlab.arthurfnsc.ifood.cadastro.infra;

import java.util.Optional;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.openapi.OASFilter;
import org.eclipse.microprofile.openapi.models.security.OAuthFlow;
import org.eclipse.microprofile.openapi.models.security.SecurityScheme;

public class SwaggerFilter implements OASFilter {

    @Override
    public SecurityScheme filterSecurityScheme(SecurityScheme securityScheme) {
        OAuthFlow flow = securityScheme.getFlows().getPassword();
        Config config = ConfigProvider.getConfig();
        Optional<String> tokenUrl = config.getOptionalValue(
            "openapi.oauth-flow.token-url",
            String.class
        );
        tokenUrl.ifPresent(flow::setTokenUrl);
        return securityScheme;
    }
}
