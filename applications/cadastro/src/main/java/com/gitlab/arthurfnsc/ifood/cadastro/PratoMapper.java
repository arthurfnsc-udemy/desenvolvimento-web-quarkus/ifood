package com.gitlab.arthurfnsc.ifood.cadastro;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.openapi.v1.restaurantes.model.CadastroPrato;

@Mapper(componentModel = "cdi")
public interface PratoMapper {
    @Mappings(
        value = {
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "restaurante", ignore = true),
        }
    )
    Prato paraPrato(CadastroPrato cadastroPrato);

    org.openapi.v1.restaurantes.model.Prato paraPratoApi(Prato prato);

    List<org.openapi.v1.restaurantes.model.Prato> paraListaPratoApi(
        List<Prato> pratos
    );
}
